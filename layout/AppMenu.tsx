/* eslint-disable @next/next/no-img-element */

import { useContext } from 'react';
import { AppMenuItem } from '../types/types';
import AppMenuitem from './AppMenuitem';
import { LayoutContext } from './context/layoutcontext';
import { MenuProvider } from './context/menucontext';

const AppMenu = () => {
    const { layoutConfig } = useContext(LayoutContext);

    const manageModel: AppMenuItem[] = [
        {
            label: 'Главная',
            items: [
                {
                    label: 'Статистика',
                    icon: 'pi pi-fw pi-chart-bar',
                    to: '/'
                },
                {
                    label: 'Сотрудники',
                    icon: 'pi pi-fw pi-users',
                    to: '/employees'
                },
            ]
        },
        {
            label: 'Локации',
            items: [
                {
                    label: 'Базовые',
                    icon: 'pi pi-fw pi-map-marker',
                    to: '/bases'
                },
                {
                    label: 'Агентские',
                    icon: 'pi pi-fw pi-map-marker',
                    to: '/agency'
                },
            ]
        },
        {
            label: 'Задачи',
            items: [
                {
                    label: 'Задачи',
                    icon: 'pi pi-fw pi-file',
                    to: '/tasks'
                },
                {
                    label: 'Задания',
                    icon: 'pi pi-fw pi-check-square',
                    to: '/jobs'
                }
            ]
        },
    ];

    return (
        <MenuProvider>
            <ul className="layout-menu">
                {manageModel.map((item, i) => {
                    return !item?.seperator ? <AppMenuitem item={item} root={true} index={i} key={item.label} /> : <li className="menu-separator"></li>;
                })}
            </ul>
        </MenuProvider>
    );
};

export default AppMenu;
