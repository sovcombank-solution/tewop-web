import { Chart } from "primereact/chart";
import { useEffect, useState } from "react";

export default function Home() {
    const [loading, setLoading] = useState({ tasks: true, employees: true, locations: true })
    const [stats, setStats] = useState({ tasks: 0, employees: 0, locations: 0 });
    useEffect(() => {
        fetch('/api/api/v1/tasks', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
                'Access-Control-Allow-Origin': process.env.ORIGIN
            }
        })
            .then(res => res.json())
            .then(res => {
                setStats(prev => ({ ...prev, tasks: res.length }))
                setLoading(prev => ({ ...prev, tasks: false }))
            })

        fetch('/api/api/v1/locations', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                'Access-Control-Allow-Origin': process.env.ORIGIN
            }
        })
            .then(res => res.json())
            .then(res => {
                setStats(prev => ({ ...prev, locations: res.length }))
                setLoading(prev => ({ ...prev, locations: false }))
            })

        fetch('/api/api/v1/employees', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
                'Access-Control-Allow-Origin': process.env.ORIGIN
            }
        })
            .then(res => res.json())
            .then(res => {
                setStats(prev => ({ ...prev, employees: res.content.length }))
                setLoading(prev => ({ ...prev, employees: false }))
            })

    }, []);

    return (
        stats.employees || stats.locations || stats.tasks ?
            <div className="layout-main-container">
                {!loading.tasks && !loading.employees && !loading.locations ?
                    <div className="card flex flex-column align-items-center">
                        <Chart type="pie" data={{
                            labels: ['Сотрудники', 'Агентские локации', 'Задачи'],
                            datasets: [
                                {
                                    data: [stats.employees, stats.locations, stats.tasks],
                                    backgroundColor: ['#6366f1', '#a855f7', '#14b8a6'],
                                    hoverBackgroundColor: ['#8183f4', '#b975f9', '#41c5b7']
                                }
                            ]
                        }} options={{
                            plugins: {
                                legend: {
                                    labels: {
                                        usePointStyle: true,
                                        color: 'black'
                                    }
                                }
                            }
                        }}></Chart>
                    </div> : <h1>Загрузка...</h1>}
            </div>
            :
            <h1>Недостаточно данных для статистики.</h1>
    );
};
