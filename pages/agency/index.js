'use client';
/* eslint-disable @next/next/no-img-element */
import { FullscreenControl, GeolocationControl, Map, Placemark, RouteButton, ZoomControl } from '@pbe/react-yandex-maps';
import { useRouter } from 'next/navigation';
import { Button } from 'primereact/button';
import { Checkbox } from 'primereact/checkbox';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { useEffect, useState } from 'react';

export default function Agency() {
    const [token, setToken] = useState('');
    const router = useRouter();

    const mockData = {
        "newPlace": true,
        "takenIntoAccountWhenPlanning": true
    }

    useEffect(() => {
        if (+localStorage.getItem('expiration') + 36000000 < Date.now()) {
            localStorage.removeItem('expiration');
            localStorage.removeItem('token');
            router.push("/")
        }
        setToken(localStorage.getItem('token'));
    }, [router])

    const [agency, setAgency] = useState([]);
    const fetchAgency = () => {
        fetch('/api/api/v1/locations', {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                'Access-Control-Allow-Origin': process.env.ORIGIN
            }
        })
            .then(res => res.json())
            .then(res => setAgency(res))
    }
    useEffect(() => {
        if (token) {
            fetchAgency();
        }
    }, [token])

    const addressBodyTemplate = (rowData) => {

        return (
            <div className="flex align-items-center gap-2">
                {rowData.address}
            </div>
        );
    };

    const [popupInfoLocation, setPopupInfoLocation] = useState(false);
    const [infoLocation, setInfoLocation] = useState({});
    const openMapBodyTemplate = (rowData) => {

        return (
            <div className="flex align-items-center gap-2">
                <a href='#' onClick={(e) => {
                    e.preventDefault();
                    setPopupInfoLocation(true);
                    setInfoLocation(rowData);
                }}>Открыть</a>
            </div>
        );
    };

    const [popupAddLocation, setPopupAddLocation] = useState(false);
    const addLocation = () => {
        setPopupAddLocation(true);
    }

    const [geo, setGeo] = useState([45.035470, 38.975313]);
    const [address, setAddress] = useState('');
    const clickMap = (e) => {
        setGeo(e.get('coords').map(e => +e.toFixed(6)));
        const coords = e.get('coords').reverse().map(e => +e.toFixed(6)).join(',');
        fetch(`https://geocode-maps.yandex.ru/1.x/?apikey=${'658da25f-4d92-4e35-bc04-c08cac3194d8'}&geocode=${coords}&kind=house&format=json`)
            .then(res => res.json())
            .then(res => setAddress(`${res.response.GeoObjectCollection.featureMember?.[0]?.GeoObject?.name}, ${res.response.GeoObjectCollection.featureMember?.[0]?.GeoObject?.metaDataProperty?.GeocoderMetaData?.Address?.Components?.find(el => el.kind === 'locality')?.name}`));
    }
    const [extra, setExtra] = useState({
        cardsAndDocumentsDelivered: true,
        daysAfterIssuance: 0,
        approvedApplications: 0,
        cardsIssued: 0,
    })

    const saveNewLocation = () => {
        const body = {
            address,
            id: agency.length,
            externalId: agency.length,
            longitude: geo[1],
            latitude: geo[0],
        }
        fetch("/api/api/v1/locations", {
            body: JSON.stringify({ ...body, ...mockData, ...extra }),
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json"
            },
            method: "POST"
        })
            .then(res => {
                if (res.status === 200) {
                    fetchAgency();
                    setPopupAddLocation(false);
                    console.log(agency)
                }
            })
    }

    const removeLocation = (id) => {
        fetch(`/api/api/v1/locations/${id}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
            method: "DELETE"
        })
            .then(res => {
                if (res.status === 200) {
                    fetchAgency();
                    setPopupInfoLocation(false);
                }
            })
    }


    const [popupEditLocation, setPopupEditLocation] = useState(false);
    const editLocation = (info) => {
        setEditData({ ...info, geo: [info.latitude, info.longitude] });
        setPopupEditLocation(true);
    }

    const [editData, setEditData] = useState({});
    const clickEditMap = (e) => {
        setEditData(prev => ({ ...prev, geo: e.get('coords').map(e => +e.toFixed(6)) }));
        const coords = [...e.get('coords')].reverse().map(e => +e.toFixed(6)).join(',');
        fetch(`https://geocode-maps.yandex.ru/1.x/?apikey=${'658da25f-4d92-4e35-bc04-c08cac3194d8'}&geocode=${coords}&kind=house&format=json`)
            .then(res => res.json())
            .then(res => setEditData(prev => ({ ...prev, address: `${res.response.GeoObjectCollection.featureMember?.[0]?.GeoObject?.name}, ${res.response.GeoObjectCollection.featureMember?.[0]?.GeoObject?.metaDataProperty?.GeocoderMetaData?.Address?.Components?.find(el => el.kind === 'locality')?.name}` })));
    }

    const saveEditLocation = () => {
        fetch("/api/api/v1/locations", {
            body: JSON.stringify({ ...editData, ...mockData }),
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json"
            },
            method: "POST"
        })
            .then(res => {
                if (res.status === 200) {
                    fetchAgency();
                    setPopupEditLocation(false);
                    setPopupInfoLocation(false);
                }
            })
    }

    const jobCompleteBodyTemplate = (rowData) => {
        return (
            <div className="flex align-items-center gap-2">
                {rowData.cardsAndDocumentsDelivered ? 'Да' : 'Нет'}
            </div>
        );
    }

    const lastCardBodyTemplate = (rowData) => {
        return (
            <div className="flex align-items-center gap-2">
                {rowData.daysAfterIssuance}
            </div>
        );
    }

    const countBidsBodyTemplate = (rowData) => {
        return (
            <div className="flex align-items-center gap-2">
                {rowData.approvedApplications}
            </div>
        );
    }

    const countCardsBodyTemplate = (rowData) => {
        return (
            <div className="flex align-items-center gap-2">
                {rowData.cardsIssued}
            </div>
        );
    }

    return (
        <>
            <Dialog visible={popupInfoLocation} style={{ width: '730px' }} header={infoLocation.address} modal onHide={() => setPopupInfoLocation(false)}>
                <Map width={680} defaultState={{ center: [infoLocation.latitude, infoLocation.longitude], zoom: 13 }}>
                    <FullscreenControl />
                    <GeolocationControl options={{ float: "left" }} />
                    <RouteButton options={{ float: "right" }} />
                    <ZoomControl options={{ float: "right" }} />
                    <Placemark defaultGeometry={[infoLocation.latitude, infoLocation.longitude]} options={{
                        iconLayout: 'default#image',
                        iconImageHref: './Sovcombank.svg',
                        iconColor: '#4338CA'
                    }} />
                </Map>
                <p className="mt-2">{`${infoLocation.latitude}, ${infoLocation.longitude}`}</p>
                <p className="mt-2">{`Кол-во дней после последней выданной карты: ${infoLocation.daysAfterIssuance}`}</p>
                <p className="mt-2">{`Кол-во одобренных заявок: ${infoLocation.approvedApplications}`}</p>
                <p className="mt-2">{`Кол-во выданных карт: ${infoLocation.cardsIssued}`}</p>
                <p className="mt-2">{`Карты и материалы доставлены: ${infoLocation.cardsAndDocumentsDelivered ? 'Да' : 'Нет'}`}</p>
                <Button label="Изменить" className="mt-2 mr-2 p-button" onClick={() => editLocation(infoLocation)}></Button>
                <Button label="Удалить" className="mt-2 p-button-danger" onClick={() => removeLocation(infoLocation.id)}></Button>
            </Dialog>
            <Dialog visible={popupAddLocation} style={{ width: '730px' }} header="Добавить агентскую локацию" modal onHide={() => setPopupAddLocation(false)}>
                {address && <p className="mb-2">{address}</p>}
                <Map
                    width={680}
                    defaultState={{
                        center: geo,
                        zoom: 13
                    }}
                    onClick={clickMap}
                >
                    <FullscreenControl />
                    <GeolocationControl options={{ float: "left" }} />
                    <ZoomControl options={{ float: "right" }} />
                    <Placemark geometry={geo} options={{
                        iconColor: '#4338CA'
                    }} />
                </Map>
                <div className="mt-2">
                    <InputText id="lastCard" placeholder="Кол-во дней после последней выданной карты" type="number" className="md:w-30rem" onChange={(e) => setExtra(prev => ({ ...prev, daysAfterIssuance: e.target.value }))} />
                </div>
                <div className="mt-2">
                    <InputText id="countBids" placeholder="Кол-во одобренных заявок" type="number" className="md:w-30rem" onChange={(e) => setExtra(prev => ({ ...prev, approvedApplications: e.target.value }))} />
                </div>
                <div className="mt-2">
                    <InputText id="countCards" placeholder="Кол-во выданных карт" type="number" className="md:w-30rem" onChange={(e) => setExtra(prev => ({ ...prev, cardsIssued: e.target.value }))} />
                </div>
                <div className="mt-2 mb-2">
                    <Checkbox inputId="cardsAndDocumentsDelivered" checked={extra.cardsAndDocumentsDelivered} onChange={(e) => setExtra(prev => ({ ...prev, cardsAndDocumentsDelivered: e.target.checked }))} className="mr-2"></Checkbox>
                    <label htmlFor="cardsAndDocumentsDelivered">Карты и материалы доставлены?</label>
                </div>
                <Button label="Сохранить" className="mt-2" onClick={saveNewLocation}></Button>
            </Dialog>
            <Dialog visible={popupEditLocation} style={{ width: '730px' }} header="Изменить агентскую локацию" modal onHide={() => setPopupEditLocation(false)}>
                <p className="mb-2">{editData.address}</p>
                <Map
                    width={680}
                    defaultState={{
                        center: editData.geo,
                        zoom: 13
                    }}
                    onClick={clickEditMap}
                >
                    <FullscreenControl />
                    <GeolocationControl options={{ float: "left" }} />
                    <ZoomControl options={{ float: "right" }} />
                    <Placemark geometry={editData.geo} options={{
                        iconColor: '#4338CA'
                    }} />
                </Map>
                <div className="mt-2">
                    <InputText value={editData.daysAfterIssuance} id="lastCard" placeholder="Кол-во дней после последней выданной карты" type="number" className="md:w-30rem" onChange={(e) => setEditData(prev => ({ ...prev, daysAfterIssuance: e.target.value }))} />
                </div>
                <div className="mt-2">
                    <InputText value={editData.approvedApplications} id="countBids" placeholder="Кол-во одобренных заявок" type="number" className="md:w-30rem" onChange={(e) => setEditData(prev => ({ ...prev, approvedApplications: e.target.value }))} />
                </div>
                <div className="mt-2">
                    <InputText value={editData.cardsIssued} id="countCards" placeholder="Кол-во выданных карт" type="number" className="md:w-30rem" onChange={(e) => setEditData(prev => ({ ...prev, cardsIssued: e.target.value }))} />
                </div>
                <div className="mt-2 mb-2">
                    <Checkbox inputId="cardsAndDocumentsDelivered" checked={editData.cardsAndDocumentsDelivered} onChange={(e) => setEditData(prev => ({ ...prev, cardsAndDocumentsDelivered: e.target.checked }))} className="mr-2"></Checkbox>
                    <label htmlFor="cardsAndDocumentsDelivered">Карты и материалы доставлены?</label>
                </div>
                <Button label="Изменить" className="mt-2" onClick={saveEditLocation}></Button>
            </Dialog>
            <div className="layout-main-container">
                <div className="table">
                    <Button label="Добавить" className="mb-4" onClick={addLocation}></Button>
                    <DataTable value={agency} paginator rows={10} showGridlines dataKey="id" emptyMessage="Нет агентских локаций">
                        <Column header="Адрес" style={{ minWidth: '12rem' }} body={addressBodyTemplate} />
                        <Column header="Карты и материалы доставлены?" style={{ minWidth: '10rem' }} body={jobCompleteBodyTemplate} />
                        <Column header="Кол-во дней после последней выданной карты" style={{ minWidth: '10rem' }} body={lastCardBodyTemplate} />
                        <Column header="Кол-во одобренных заявок" style={{ minWidth: '10rem' }} body={countBidsBodyTemplate} />
                        <Column header="Кол-во выданных карт" style={{ minWidth: '10rem' }} body={countCardsBodyTemplate} />
                        <Column header="Открыть на карте" style={{ minWidth: '6rem' }} body={openMapBodyTemplate} />
                    </DataTable>
                </div>
            </div>
        </>
    );
};
