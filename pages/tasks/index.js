'use client';
/* eslint-disable @next/next/no-img-element */
import { FullscreenControl, GeolocationControl, Map, Placemark, RouteButton, ZoomControl } from '@pbe/react-yandex-maps';
import { useRouter } from 'next/navigation';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { Dialog } from 'primereact/dialog';
import { Tag } from 'primereact/tag';
import { useEffect, useState } from 'react';

export default function Tasks() {
    const [token, setToken] = useState('');
    const router = useRouter();

    useEffect(() => {
        if (+localStorage.getItem('expiration') + 36000000 < Date.now()) {
            localStorage.removeItem('expiration');
            localStorage.removeItem('token');
            router.push("/")
        }
        setToken(localStorage.getItem('token'));
    }, [router])

    const [tasks, setTasks] = useState([
        {
            id: 1,
            "assigned": true,
            "canceled": true,
            "overdue": true,
            "currentPriority": {
                "id": 1,
                "name": "HIGH",
                "levelName": "Высокий"
            },
            "completed": true,
            "location": {
                "id": 0,
                "externalId": 0,
                "address": "Краснодар, улица Сбер 123",
                "longitude": 0,
                "latitude": 0,
                "newPlace": true,
                "cardsAndDocumentsDelivered": true,
                "daysAfterIssuance": 0,
                "approvedApplications": 0,
                "cardsIssued": 0,
                "takenIntoAccountWhenPlanning": true
            },
            "taskType": {
                "id": 1,
                "name": "Выезд на точку для стимулирования выдач",
                "priority": {
                    "id": 1,
                    "name": "HIGH",
                    "levelName": "Высокий"
                },
                "duration": 240,
                "minimumDegree": {
                    "id": 3,
                    "name": "SENIOR",
                    "preferredName": "Старший специалист"
                }
            },
        }]);
    const fetchTasks = () => {
        fetch('/api/api/v1/tasks', {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Access-Control-Allow-Origin': process.env.ORIGIN
            }
        })
            .then(res => res.json())
            .then(res => console.log(res))
    }
    const [types, setTypes] = useState([]);
    const fetchTypes = () => {
        fetch('/api/api/v1/task-types', {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Access-Control-Allow-Origin': process.env.ORIGIN
            }
        })
            .then(res => res.json())
            .then(res => {
                console.log(res)
                return res
            })
            .then(res => setTypes(res))
    }
    useEffect(() => {
        if (token) {
            fetchTasks();
            fetchTypes();
        }
    }, [token])

    const taskBodyTemplate = (rowData) => {
        return (
            <div className="flex align-items-center gap-2">
                {rowData?.taskType?.name}
            </div>
        );
    };

    const completeBodyTemplate = (rowData) => {
        return (
            <div className="flex align-items-center gap-2">
                {rowData?.completed && <i className="layout-menuitem-icon pi pi-fw pi-check" title="Готова"></i>}
                {rowData?.assigned && <i className="layout-menuitem-icon pi pi-fw pi-user-plus" title="Назначена"></i>}
                {rowData?.canceled && <i className="layout-menuitem-icon pi pi-fw pi-times" title="Отменена"></i>}
                {rowData?.overdue && <i className="layout-menuitem-icon pi pi-fw pi-ban" title="Просрочена"></i>}
            </div>
        );
    };

    const priorityBodyTemplate = (rowData) => {
        const priority = rowData?.currentPriority?.name;
        const map = {
            LOW: "success",
            MIDDLE: "warning",
            HIGH: "danger",
        }
        return (
            <div className="flex align-items-center gap-2">
                <Tag severity={map[priority]} value={rowData?.currentPriority?.levelName}></Tag>
            </div>
        );
    };

    const [popupInfoTasks, setPopupInfoTasks] = useState(false);
    const [infoTasks, setInfoTasks] = useState({});
    const openMapBodyTemplate = (rowData) => {
        return (
            <div className="flex align-items-center gap-2">
                <a href='#' onClick={(e) => {
                    e.preventDefault();
                    setInfoTasks(rowData);
                    setPopupInfoTasks(true);
                }}>{rowData?.location?.address}</a>
            </div>
        );
    };

    return (
        <>
            <Dialog visible={popupInfoTasks} style={{ width: '730px' }} header={infoTasks?.taskType?.name} modal onHide={() => setPopupInfoTasks(false)}>
                <Map width={680} defaultState={{ center: [infoTasks?.location?.latitude, infoTasks?.location?.longitude], zoom: 13 }}>
                    <FullscreenControl />
                    <GeolocationControl options={{ float: "left" }} />
                    <RouteButton options={{ float: "right" }} />
                    <ZoomControl options={{ float: "right" }} />
                    <Placemark defaultGeometry={[infoTasks?.location?.latitude, infoTasks?.location?.longitude]} options={{
                        iconLayout: 'default#image',
                        iconImageHref: './Sovcombank.svg',
                        iconColor: '#4338CA'
                    }} />
                </Map>
                <p className="mt-2">{`${infoTasks?.location?.latitude}, ${infoTasks?.location?.longitude}`}</p>
            </Dialog>
            <div className="layout-main-container">
                <div className="table">
                    <DataTable value={tasks} paginator rows={10} showGridlines dataKey="id" emptyMessage="Нет задач.">
                        <Column header="Задача" style={{ minWidth: '10rem' }} body={taskBodyTemplate} />
                        <Column header="Статус" style={{ minWidth: '6rem' }} body={completeBodyTemplate} />
                        <Column header="Адрес" style={{ minWidth: '12rem' }} body={openMapBodyTemplate} />
                        <Column header="Приоритет" style={{ minWidth: '6rem' }} body={priorityBodyTemplate} />
                    </DataTable>
                </div>
            </div>
        </>
    );
};
