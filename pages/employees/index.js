/* eslint-disable @next/next/no-img-element */
'use client';
import { useRouter } from 'next/navigation';
import { classNames } from 'primereact/utils';
import React, { useContext, useEffect, useState } from 'react';
import { LayoutContext } from '../../layout/context/layoutcontext';

import getCoordinates from '@/utils/get-coordinates';
import { FullscreenControl, GeolocationControl, Map, Placemark, RouteButton, ZoomControl } from '@pbe/react-yandex-maps';
import { FilterMatchMode } from 'primereact/api';
import { Button } from 'primereact/button';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { Dialog } from 'primereact/dialog';
import { Dropdown } from 'primereact/dropdown';
import { InputText } from 'primereact/inputtext';
import { Tag } from 'primereact/tag';
import styles from './employees.module.css';


const EmployeesPage = () => {
    const { layoutConfig } = useContext(LayoutContext);

    const [token, setToken] = useState('');
    const router = useRouter();

    useEffect(() => {
        if (+localStorage.getItem('expiration') + 36000000 < Date.now()) {
            localStorage.removeItem('expiration');
            localStorage.removeItem('token');
            router.push("/")
        }
        setToken(localStorage.getItem('token'));
    }, [router])

    
    const containerClassName = classNames('surface-ground flex align-items-center justify-content-center min-h-screen min-w-screen overflow-hidden', { 'p-input-filled': layoutConfig.inputStyle === 'filled' });
    
    const [authEmployees, setAuthEmployees] = useState([{
        "id": "1",
        "personnelNumber": "001",
        "firstName": "Никита",
        "lastName": "Дерягин",
        "patronymic": "Владимирович",
        "email": "deriagin@mail.com",
        "avatar": "string",
        "degreeId": 2,
        "baseLocationId": 0,
        "employee": true,
        "available": true,
        "agent": false
    }]);

    const [tableEmployees, setTableEmployees] = useState([{
        "id": "1",
        "personnelNumber": "001",
        "firstName": "Никита",
        "lastName": "Дерягин",
        "patronymic": "Владимирович",
        "email": "deriagin@mail.com",
        "avatar": "string",
        "degreeId": 2,
        "baseLocationId": 0,
        "employee": true,
        "available": true,
        "agent": false
    }]);

    const [popupUpdateEmployee, setPopupUpdateEmployee] = useState(false);
    const [filters, setFilters] = useState({
        global: { value: null, matchMode: FilterMatchMode.CONTAINS },
    });
    const [loading, setLoading] = useState(false);
    const [popupInfoEmployee, setPopupInfoEmployee] = useState(false);
    const [geo, setGeo] = useState([0, 0]);
    const [infoEmployee, setinfoEmployee] = useState({});
    const [isLoadingInfo, setIsLoadingInfo] = useState(false);
    const [globalFilterValue, setGlobalFilterValue] = useState('');
    const [updateEmployee, setUpdateEmployee] = useState({});

    // useEffect(() => {
    //     try {
    //         fetch('/api/api/v1/employees', {
    //             headers: {
    //                 'Authorization': `Bearer ${token}`,
    //                 'Access-Control-Allow-Origin': process.env.ORIGIN
    //             } 
    //         }).then((res) => res.json()).then((res) => setAuthEmployees(res.content))
    //     } catch (e) {
    //         console.error(e)
    //     }
    // }, [])

    const [baseLocations, setBaseLocations] = useState([]);

    const fetchBaseLocations = () => {
        fetch('/api/api/v1/base-locations', {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                'Access-Control-Allow-Origin': process.env.ORIGIN
            }
        })
            .then(res => res.json())
            .then(res => setBaseLocations(res))
    }
    useEffect(() => {
        if (token) {
            fetchBaseLocations();
        }
    }, [token])
    
    const [grades] = useState(['junior', 'middle', 'senior']);
    
    const getSeverity = (grade) => {
        switch (grade) {
            case 'senior':
                return 'danger';
            
            case 'middle':
                return 'warning';
            
            case 'junior':
                return 'success';
        }
    };
    
    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };

        _filters['global'].value = value;

        setFilters(_filters);
        setGlobalFilterValue(value);
    };

    const initFilters = () => {
        setFilters({
            global: { value: null, matchMode: FilterMatchMode.CONTAINS },
        });
        setGlobalFilterValue('');
    };

    const addressBodyTemplate = (rowData) => {
        let base = baseLocations[rowData.baseLocationId]
        return (
            <div className="flex align-items-center gap-2">
                {base?.address}
            </div>
        );
    };

    const RenderHeader = () => {
        const [visible, setVisible] = useState(false);

        const [selectedEmployee, setSelectedEmployee] = useState('')
        const employeesFio = []

        for (let employee of authEmployees) {
            employeesFio.push(`${employee.lastName} ${employee.firstName} ${employee.patronymic}`)
        }
        
        const addEmployee = () => {
            for (let employee of authEmployees){
                let fio = `${employee.lastName} ${employee.firstName} ${employee.patronymic}`
                if (fio == selectedEmployee){
                    try{
                        tableEmployees.push(employee)
                    }catch (e) {
                        console.log(e)
                    }
                    return
                }
            }
            
        }

        const addEmployeeHeader = (
            <div className='flex '>
                <span className='flex' >
                    <p>Добавление нового сотрудника</p>
                </span>
                
                <Button label='Сохранить' onClick={() => {addEmployee(); setVisible(false)}} />
            </div>
        )

        const selectedDegreeTemplate = (option, props) => {
            if (option) {
                return (
                    <Tag value={option} severity={getSeverity(option)} />
                );
            }
            return <span>{props.placeholder}</span>;
        };
        return (
            <div className="flex justify-content-between">
                <h2>Сотрудники</h2>
                <span className="p-input-icon-left">
                    <i className="pi pi-search"/>
                    <InputText value={globalFilterValue} onChange={onGlobalFilterChange} placeholder="Keyword Search" />
                </span>
                <Button label="Добавить сотрудника" onClick={() => setVisible(true)} />
                <Dialog className={styles.dialog} header={addEmployeeHeader} visible={visible} style={{ width: '60vw' }} onHide={() => setVisible(false)}>
                    <label htmlFor="fio" className="block text-900 text-xl font-medium mb-2">
                        Выбрать сотрудника
                    </label>
                    <Dropdown value={selectedEmployee} onChange={(e) => setSelectedEmployee(e.value)} options={employeesFio} optionLabel="name" 
                        editable placeholder="Выбрать сотрудника" className="w-full md:w-14rem" />
                </Dialog>
            </div>
        );
    };

    const updateEmployeeDialog = () => {

    }
    const removeEmployee = (id) => {
        fetch(`/api/api/v1/employees/${id}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
            method: "DELETE"
        })
            .then(res => {
                if (res.status === 200) {
                    setPopupInfoLocation(false);
                }
            })
    }

    const representativeBodyTemplate = (rowData) => {
        const representative = `${rowData.lastName} ${rowData.firstName} ${rowData.patronymic}`;

        return (
            <div className="flex align-items-center gap-2">
                <Button icon="pi pi-pencil" onClick={() => {setUpdateEmployee(rowData); setPopupUpdateEmployee(true);}} rounded text severity="secondary" aria-label="pensil" />
                <a href='#' onClick={(e) => {
                    e.preventDefault();
                    setIsLoadingInfo(true);
                    setPopupInfoEmployee(true);
                    setinfoEmployee(rowData);
                    getCoordinates(baseLocations[rowData.baseLocationId].address).then(res => {
                        setGeo(res);
                        console.log(res);
                        setIsLoadingInfo(false);
                    });
                }}>{representative}</a>
            </div>
        );
    };

    const gradeBodyTemplate = (rowData) => {
        return <Tag value={grades[rowData.degreeId]} severity={getSeverity(grades[rowData.degreeId])} />;
    };

    const header = RenderHeader();
    console.log(authEmployees)
    return (
        <>
            <Dialog visible={popupInfoEmployee} style={{ width: '730px' }} header={`${infoEmployee.lastName} ${infoEmployee.firstName} ${infoEmployee.patronymic}`} modal onHide={() => setPopupInfoEmployee(false)}>
                <b>Маршрут</b>
                {!isLoadingInfo && <Map defaultState={{ center: geo, zoom: 13 }}>
                    <FullscreenControl />
                    <GeolocationControl options={{ float: "left" }} />
                    <RouteButton options={{ float: "right" }} />
                    <ZoomControl options={{ float: "right" }} />
                    <Placemark defaultGeometry={geo} options={{
                        iconLayout: 'default#image',
                        iconImageHref: './Sovcombank.svg',
                        iconColor: '#4338CA'
                    }} />
                </Map>}
            </Dialog>
            <div className="layout-main-container">
                <DataTable value={tableEmployees} paginator showGridlines rows={10} loading={loading} dataKey="id"
                    filters={filters} globalFilterFields={['', 'address', '', 'grade']} header={header}
                    emptyMessage="Нет сотрудников">
                    <Column header="ФИО" style={{ minWidth: '14rem' }} body={representativeBodyTemplate} />
                    <Column field="personnelNumber" header="Табельный номер" style={{ minWidth: '12rem' }} />
                    <Column header="Адрес" style={{ minWidth: '12rem' }} body={addressBodyTemplate}/>
                    <Column field="degreeId" header="Грейд" style={{ minWidth: '12rem' }} body={gradeBodyTemplate} />
                </DataTable>
            </div>
            <Dialog visible={popupUpdateEmployee} style={{ width: '730px' }} header={`${updateEmployee.lastName} ${updateEmployee.firstName} ${updateEmployee.patronymic}`} 
                modal onHide={() => setPopupUpdateEmployee(false)} >
                <div className=''>
                    <label htmlFor='firstName'>
                        ФИО
                    </label>

                    <span className='flex justify-content-between'>
                        <InputText id="firstName" value={updateEmployee.firstName} placeholder='Имя' onChange={(e) => {updateEmployee.firstName = e.target.value}}/>
                        <InputText id="lastName" value={updateEmployee.lastName} placeholder='Фамилия' onChange={(e) => {updateEmployee.lastName = e.target.value}}/>
                        <InputText id="patronymic" value={updateEmployee.patronymic} placeholder='Отчество' onChange={(e) => {updateEmployee.patronymic = e.target.value}}/>
                    </span>

                    <div className='flex justify-content-between'>
                        <span>
                            <label htmlFor='personnelNumber'>
                                Табельный номер
                            </label>

                            <InputText id="personnelNumber" value={updateEmployee.personnelNumber} placeholder='Номер' onChange={(e) => {updateEmployee.personnelNumber = e.target.value}}/>
                        </span>

                        <span>
                            <label htmlFor='baseLocations'>
                                Адрес
                            </label>

                            <Dropdown id="baseLocations" value={updateEmployee.baseLocationId} placeholder='Номер' onChange={(e) => {updateEmployee.baseLocationId = e.target.value}} options={baseLocations} optionLabel="address"/>
                        </span>
                    </div>
                    <label htmlFor='degree'>
                        Грейд
                    </label>

                    <InputText id="degree" value={updateEmployee.personnelNumber} placeholder='Номер' onChange={(e) => {updateEmployee.personnelNumber = e.target.value}}/>
                    
                    <Button label="Изменить" className="mt-2 mr-2 p-button" onClick={() => updateEmployee(updateEmployee)}></Button>
                    <Button label="Удалить" className="mt-2 p-button-danger" onClick={() => removeEmployee(updateEmployee.id)}></Button>
                </div>
            </Dialog>
        </>
    );
}

export default EmployeesPage;
