'use client';
/* eslint-disable @next/next/no-img-element */
import { FullscreenControl, GeolocationControl, Map, Placemark, RouteButton, ZoomControl } from '@pbe/react-yandex-maps';
import { useRouter } from 'next/navigation';
import { Button } from 'primereact/button';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { Dialog } from 'primereact/dialog';
import { InputTextarea } from 'primereact/inputtextarea';
import { useEffect, useState } from 'react';

export default function Bases() {
    const [token, setToken] = useState('');
    const router = useRouter();

    useEffect(() => {
        if (+localStorage.getItem('expiration') + 36000000 < Date.now()) {
            localStorage.removeItem('expiration');
            localStorage.removeItem('token');
            router.push("/")
        }
        setToken(localStorage.getItem('token'));
    }, [router])

    const [bases, setBases] = useState([]);
    const fetchBases = () => {
        fetch('/api/api/v1/base-locations', {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                'Access-Control-Allow-Origin': process.env.ORIGIN
            }
        })
            .then(res => res.json())
            .then(res => setBases(res))
    }
    useEffect(() => {
        if (token) {
            fetchBases();
        }
    }, [token])

    const addressBodyTemplate = (rowData) => {
        return (
            <div className="flex align-items-center gap-2">
                {rowData.address}
            </div>
        );
    };

    const longitudeBodyTemplate = (rowData) => {

        return (
            <div className="flex align-items-center gap-2">
                {rowData.longitude}
            </div>
        );
    };

    const latitudeBodyTemplate = (rowData) => {

        return (
            <div className="flex align-items-center gap-2">
                {rowData.latitude}
            </div>
        );
    };

    const [popupInfoLocation, setPopupInfoLocation] = useState(false);
    const [infoLocation, setInfoLocation] = useState({});
    const openMapBodyTemplate = (rowData) => {

        return (
            <div className="flex align-items-center gap-2">
                <a href='#' onClick={(e) => {
                    e.preventDefault();
                    setPopupInfoLocation(true);
                    setInfoLocation(rowData);
                }}>Открыть</a>
            </div>
        );
    };

    const [popupAddLocation, setPopupAddLocation] = useState(false);
    const addLocation = () => {
        setPopupAddLocation(true);
    }

    const [geo, setGeo] = useState([45.035470, 38.975313]);
    const [isGeoChanged, setIsGeoChanged] = useState(false);
    const [address, setAddress] = useState('');
    const clickMap = (e) => {
        setGeo(e.get('coords').map(e => +e.toFixed(6)));
        setIsGeoChanged(true);
        const coords = e.get('coords').reverse().map(e => +e.toFixed(6)).join(',');
        fetch(`https://geocode-maps.yandex.ru/1.x/?apikey=${'658da25f-4d92-4e35-bc04-c08cac3194d8'}&geocode=${coords}&kind=house&format=json`)
            .then(res => res.json())
            .then(res => setAddress(`${res.response.GeoObjectCollection.featureMember?.[0]?.GeoObject?.name}, ${res.response.GeoObjectCollection.featureMember?.[0]?.GeoObject?.metaDataProperty?.GeocoderMetaData?.Address?.Components?.find(el => el.kind === 'locality')?.name}`));
    }

    const [addDescription, setAddDescription] = useState('');
    const onTextareaChange = (e) => setAddDescription(e.target.value);

    const saveNewLocation = () => {
        const body = {
            description: addDescription,
            address,
            id: bases.length,
            longitude: geo[1],
            latitude: geo[0],
        }
        fetch("/api/api/v1/base-locations", {
            body: JSON.stringify(body),
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json"
            },
            method: "POST"
        })
            .then(res => {
                if (res.status === 200) {
                    fetchBases();
                    setPopupAddLocation(false);
                }
            })
    }

    const removeLocation = (id) => {
        fetch(`/api/api/v1/base-locations/${id}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
            method: "DELETE"
        })
            .then(res => {
                if (res.status === 200) {
                    fetchBases();
                    setPopupInfoLocation(false);
                }
            })
    }


    const [popupEditLocation, setPopupEditLocation] = useState(false);
    const editLocation = (info) => {
        setEditData({ ...info, geo: [info.latitude, info.longitude] });
        setPopupEditLocation(true);
    }

    const [editData, setEditData] = useState({});
    const clickEditMap = (e) => {
        setEditData(prev => ({ ...prev, geo: e.get('coords').map(e => +e.toFixed(6)) }));
        const coords = [...e.get('coords')].reverse().map(e => +e.toFixed(6)).join(',');
        fetch(`https://geocode-maps.yandex.ru/1.x/?apikey=${'658da25f-4d92-4e35-bc04-c08cac3194d8'}&geocode=${coords}&kind=house&format=json`)
            .then(res => res.json())
            .then(res => setEditData(prev => ({ ...prev, address: `${res.response.GeoObjectCollection.featureMember?.[0]?.GeoObject?.name}, ${res.response.GeoObjectCollection.featureMember?.[0]?.GeoObject?.metaDataProperty?.GeocoderMetaData?.Address?.Components?.find(el => el.kind === 'locality')?.name}` })));
    }

    const onEditTextareaChange = (e) => setEditData(prev => ({ ...prev, description: e.target.value }));

    const saveEditLocation = () => {
        fetch("/api/api/v1/base-locations", {
            body: JSON.stringify(editData),
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json"
            },
            method: "POST"
        })
            .then(res => {
                if (res.status === 200) {
                    fetchBases();
                    setPopupEditLocation(false);
                    setPopupInfoLocation(false);
                }
            })
    }

    return (
        <>
            <Dialog visible={popupInfoLocation} style={{ width: '730px' }} header={infoLocation.address} modal onHide={() => setPopupInfoLocation(false)}>
                <Map width={680} defaultState={{ center: [infoLocation.latitude, infoLocation.longitude], zoom: 13 }}>
                    <FullscreenControl />
                    <GeolocationControl options={{ float: "left" }} />
                    <RouteButton options={{ float: "right" }} />
                    <ZoomControl options={{ float: "right" }} />
                    <Placemark defaultGeometry={[infoLocation.latitude, infoLocation.longitude]} options={{
                        iconLayout: 'default#image',
                        iconImageHref: './Sovcombank.svg',
                        iconColor: '#4338CA'
                    }} />
                </Map>
                <p className="mt-4">{infoLocation.description}</p>
                <Button label="Изменить" className="mt-2 mr-2 p-button" onClick={() => editLocation(infoLocation)}></Button>
                <Button label="Удалить" className="mt-2 p-button-danger" onClick={() => removeLocation(infoLocation.id)}></Button>
            </Dialog>
            <Dialog visible={popupAddLocation} style={{ width: '730px' }} header="Добавить базовую локацию" modal onHide={() => setPopupAddLocation(false)}>
                {address && <p className="mb-2">{address}</p>}
                <Map
                    width={680}
                    defaultState={{
                        center: geo,
                        zoom: 13
                    }}
                    onClick={clickMap}
                >
                    <FullscreenControl />
                    <GeolocationControl options={{ float: "left" }} />
                    <ZoomControl options={{ float: "right" }} />
                    <Placemark geometry={geo} options={{
                        iconColor: '#4338CA'
                    }} />
                </Map>
                <div className="textarea">
                    <InputTextarea className="mt-2" id="description" value={addDescription} placeholder="Введите описание" onChange={onTextareaChange} />
                </div>
                {isGeoChanged && <Button label="Сохранить" className="mt-2" onClick={saveNewLocation}></Button>}
            </Dialog>
            <Dialog visible={popupEditLocation} style={{ width: '730px' }} header="Изменить базовую локацию" modal onHide={() => setPopupEditLocation(false)}>
                <p className="mb-2">{editData.address}</p>
                <Map
                    width={680}
                    defaultState={{
                        center: editData.geo,
                        zoom: 13
                    }}
                    onClick={clickEditMap}
                >
                    <FullscreenControl />
                    <GeolocationControl options={{ float: "left" }} />
                    <ZoomControl options={{ float: "right" }} />
                    <Placemark geometry={editData.geo} options={{
                        iconColor: '#4338CA'
                    }} />
                </Map>
                <div className="textarea">
                    <InputTextarea className="mt-2" id="description" value={editData.description} placeholder="Введите описание" onChange={onEditTextareaChange} />
                </div>
                <Button label="Изменить" className="mt-2" onClick={saveEditLocation}></Button>
            </Dialog>
            <div className="layout-main-container">
                <div className="table">
                    <Button label="Добавить" className="mb-4" onClick={addLocation}></Button>
                    <DataTable value={bases} paginator rows={10} showGridlines dataKey="id" emptyMessage="Нет базовых локаций">
                        <Column header="Адрес" style={{ minWidth: '14rem' }} body={addressBodyTemplate} />
                        <Column header="Долгота" style={{ minWidth: '12rem' }} body={longitudeBodyTemplate} />
                        <Column header="Широта" style={{ minWidth: '12rem' }} body={latitudeBodyTemplate} />
                        <Column header="Открыть на карте" style={{ minWidth: '12rem' }} body={openMapBodyTemplate} />
                    </DataTable>
                </div>
            </div>
        </>
    );
};
