/* eslint-disable @next/next/no-img-element */
'use client';
import { useRouter } from 'next/navigation';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { Toast } from 'primereact/toast';
import { classNames } from 'primereact/utils';
import { useContext, useRef, useState } from 'react';
import { LayoutContext } from '../../layout/context/layoutcontext';
import styles from './login.module.css';

const LoginPage = ({ setIsAuthorized, setToken }) => {
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const { layoutConfig } = useContext(LayoutContext);

    const router = useRouter();
    const containerClassName = classNames('surface-ground flex align-items-center justify-content-center min-h-screen min-w-screen overflow-hidden', { 'p-input-filled': layoutConfig.inputStyle === 'filled' });

    const sendLoginData = async () => {
        try {
            const data = await fetch('/api/sso/realms/sovcombank/protocol/openid-connect/token', {
                body: `grant_type=password&username=${login}&password=${password}`,
                method: 'POST',
                headers: {
                    'Authorization': 'Basic dGV3b3A6NkxrZ2d3ZTRjeXZPckRpVVhPUFF6cnRyZTZNTFNxaGM=',
                    'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                    'Access-Control-Allow-Origin': process.env.ORIGIN
                }
            })
                .then(res => {
                    if (res.status === 200) {
                        return res.json()
                    } else {
                        return res.text().then(text => { throw new Error(text) })
                    }
                })
                .then((res) => {
                    localStorage.setItem("token", res.access_token);
                    localStorage.setItem("expiration", Date.now() + res.refresh_expires_in);
                    setToken(res.access_token);
                    setIsAuthorized(true);
                    router.push("/")
                })
        } catch (e) {
            setLogin('');
            setPassword('');
            reject();
        }
    }

    const toast = useRef(null);
    const reject = () => {
        toast.current?.show({
            severity: 'error',
            summary: 'Ошибка',
            detail: 'Неправильный логин или пароль',
            life: 3000
        });
    }

    return (
        <div className={containerClassName}>
            <Toast ref={toast} />
            <div className="flex flex-column align-items-center justify-content-center">
                <div className={`w-full surface-card py-8 px-5 sm:px-8 ${styles.card}`} style={{ borderRadius: '53px' }}>
                    <div className="text-center mb-5">
                        <img src="/Sovcombank.svg" alt="Image" height="73" className="mb-3" />
                        <div className="text-900 text-3xl font-medium mb-3">Добрый день</div>
                        <span className="text-600 font-medium">Войдите, чтобы продолжить</span>
                    </div>

                    <div>
                        <label htmlFor="email1" className="block text-900 text-xl font-medium mb-2">
                            Логин
                        </label>
                        <InputText id="email1" value={login} type="text" placeholder="Логин" onChange={(e) => setLogin(e.target.value)} className="w-full md:w-30rem mb-5" style={{ padding: '1rem' }} />

                        <label htmlFor="password1" className="block text-900 font-medium text-xl mb-2">
                            Пароль
                        </label>
                        <InputText inputId="password1" value={password} onChange={(e) => setPassword(e.target.value)} placeholder="Пароль" toggleMask className="w-full md:w-30rem mb-5" style={{ padding: '1rem' }} inputClassName="w-full p-3 md:w-30rem"></InputText>
                        <div className="flex align-items-center justify-content-between mb-5 gap-5">
                        </div>
                        <Button label="Войти" className="w-full p-3 text-xl" onClick={sendLoginData}></Button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LoginPage;
