import { YMaps } from '@pbe/react-yandex-maps';
import Head from 'next/head';
import 'primeflex/primeflex.css';
import 'primeicons/primeicons.css';
import { PrimeReactProvider } from 'primereact/api';
import 'primereact/resources/primereact.css';
import { LayoutProvider } from '../layout/context/layoutcontext';
import '../styles/demo/Demos.scss';
import '../styles/layout/layout.scss';
import Layout from './layout';

export default function App({ Component, pageProps }) {
    return <>
        <Head>
            <title>Anvacon</title>
        </Head>
        <PrimeReactProvider>
            <LayoutProvider>
                <YMaps query={{
                    apikey: '658da25f-4d92-4e35-bc04-c08cac3194d8'
                }}>
                    <Layout>
                        <Component {...pageProps} />
                    </Layout>
                </YMaps>
            </LayoutProvider>
        </PrimeReactProvider></>
}