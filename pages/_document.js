import { Head, Html, Main, NextScript } from 'next/document';
export default function Document() {
    return (
        <Html>
            <Head>
                <link id="theme-css" href={`/themes/lara-light-indigo/theme.css`} rel="stylesheet" />
                <link rel="icon" href="./Sovcombank.svg" />
            </Head>
            <body>
                <Main />
                <NextScript />
            </body>
        </Html>
    )
}