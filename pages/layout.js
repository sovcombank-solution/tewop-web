'use client';
/* eslint-disable @next/next/no-img-element */

import Link from 'next/link';
import { usePathname, useRouter } from "next/navigation";
import { Button } from 'primereact/button';
import { classNames } from "primereact/utils";
import { useEffect, useState } from "react";
import AppMenu from "../layout/AppMenu";
import AppTopbar from "../layout/AppTopbar";
import styles from "./index.module.css";
import LoginPage from './login';

export default function Layout({ children }) {
    const router = useRouter();
    const pathname = usePathname();
    const containerClass = classNames("layout-wrapper", {
        "layout-static": true,
    });

    const [isAuthorized, setIsAuthorized] = useState(false);
    const [token, setToken] = useState('');
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        if (+localStorage.getItem('expiration') + 36000000 < Date.now()) {
            localStorage.removeItem('expiration');
            localStorage.removeItem('token');
        }
        setIsAuthorized(!!localStorage.getItem('token'));
        setToken(localStorage.getItem('token'));
        setIsLoading(false);
    }, [])

    const [user, setUser] = useState({});

    useEffect(() => {
        if (isAuthorized) {
            try {
                fetch('/api/api/v1/authenticated-user/oidc-user-info', {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                        'Access-Control-Allow-Origin': process.env.ORIGIN
                    }
                }).then((res) => res.json()).then((res) => setUser(res))
            } catch (e) {
                console.error(e)
            }
        }
    }, [isAuthorized, token])

    if (pathname === '/login') {
        return (
            <div className={containerClass}>
                <div className="layout-main"><LoginPage setIsAuthorized={setIsAuthorized} setToken={setToken} /></div>
            </div>
        )
    }

    return (
        <div className={containerClass}>
            {isAuthorized && <AppTopbar name={user?.profile?.givenName} email={user?.profile?.email} picture={user?.profile?.picture} />}
            <div className="layout-main">
                {isLoading ? <h1>Загрузка...</h1> :
                    isAuthorized ? <>
                        <div className="layout-sidebar">
                            <AppMenu />
                        </div>
                        {children}
                    </> :
                        <div className={`flex justify-content-center align-items-center flex-column ${styles.main}`} >
                            <Link href="/">
                                <img src={`/Anvacon.svg`} alt="Logo" height="50" />
                            </Link>
                            <Button label="Авторизоваться" className={`border-none font-light line-height-2 text-white ${styles.button}`} onClick={() => router.push("/login")}></Button>
                        </div >}
            </div>
        </div >
    );
};