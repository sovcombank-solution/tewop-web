const getCoordinates = (address) => {
    const geo = address.split(' ').join('+');
    return fetch(`https://geocode-maps.yandex.ru/1.x/?apikey=${'658da25f-4d92-4e35-bc04-c08cac3194d8'}&geocode=${geo}&lang=ru_RU&format=json`)
        .then(res => res.json())
        .then(res => res.response.GeoObjectCollection.featureMember[0].GeoObject.Point.pos)
        .then(res => res.split(' ').map(e => +e).reverse())
}

export default getCoordinates;