const isDevelopment = process.env.NODE_ENV !== "production";
const rewritesConfig = isDevelopment
    ? [
        {
            source: "/api/:path*",
            destination: "http://195.93.252.223:9080/:path*",
        },
    ]
    : [];
module.exports = {
    reactStrictMode: true,
    output: "standalone",
    rewrites: async () => rewritesConfig,
};